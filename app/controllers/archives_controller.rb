require 'rubygems'
require 'zip'
require 'tempfile'

class ArchivesController < ApplicationController
  include ApplicationHelper
  
  def index
    @archives = Archive.order('created_at desc').all
  end

  def new
    refresh=true
    @archive = Archive.new
  end

  def create
    @archive_attributes = params.require(:archive).permit!
    @archive = Archive.create(@archive_attributes)
    if @archive.errors.empty? && @archive.documents.present?
      begin
        @archive.get_input_filenames
        filename = "#{@archive.name}.zip"
        zipfile_name = @archive.download_zip
      
        send_file zipfile_name, :type => 'application/zip',
                                :disposition => 'attachment',
                                :filename => filename
      rescue Exception => e
        Rails.logger.info e
        redirect_to new_archive_path, alert: e
      end
    else
      msg = @archive.errors.present? ? @archive.errors.full_messages.join(" ") : "Please add atleast one document"
      redirect_to new_archive_path, alert: msg
      return
    end
  end

  def destroy
    archive = Archive.find(params[:id])
    zipfile_path = Rails.root.join("public","zip", "#{archive.name}.zip")
    FileUtils.rm_rf(zipfile_path)
    if archive.delete
      redirect_to archives_path, :notice => "Successfully deleted"
    else
      redirect_to archives_path, :alert => "Cannot delete"
    end
  end

  def download
    archive = Archive.find(params[:id])
    zipfile_name = Rails.root.join("public","zip", "#{archive.name}.zip")
    file_name = File.open(zipfile_name, 'rb')

    t = Tempfile.new("#{archive.name}.zip")
    filename = "#{archive.name}.zip"
    send_file file_name,  :type => 'application/zip',
                          :disposition => 'attachment',
                          :filename => filename
  end

  private

  def safe_params
    safe_attributes = [
      :name,
      documents_attributes: [
        :main_file,
        :_destroy
      ]
    ]
    params.require(:archive).permit(*safe_attributes)
  end

  
end