class Archive < ActiveRecord::Base
  has_many :documents, dependent: :destroy
  validates_presence_of :name
  validates_uniqueness_of :name
  accepts_nested_attributes_for :documents, allow_destroy: true, reject_if: Proc.new{|attrs| attrs[:main_file].blank? }

  
  def get_input_filenames
    archive = self
    input_filenames = {}
    archive.documents.each do |doc|
      input_filenames[doc.id] = doc.main_file_file_name
    end
    return input_filenames
  end


  def download_zip
    archive = self
    archive_name = archive.name
    
    folder = Rails.root.join("public","system")

    input_filenames = archive.get_input_filenames
    
    zipfile_name = Rails.root.join("public","zip","#{archive_name}.zip")
    filename = "#{archive_name}.zip"
    
    t = Tempfile.new("#{archive_name}.zip")

    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      input_filenames.each do |doc_id, filename|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
        zipfile.add(filename, "#{folder}" + "/" +"#{doc_id}"+"/"+ "#{filename}")
      end
    end
    return zipfile_name
    
    t.close
  end
end
