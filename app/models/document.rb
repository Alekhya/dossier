class Document < ActiveRecord::Base
  belongs_to :archive

  has_attached_file :main_file
  
  validates_attachment :main_file, 
                        presence: true, 
                        content_type: { content_type: [ "image/jpeg", "image/png", "image/jpg", "text/csv", "text/html", "text/xml", "application/msword"] }
end
