class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :archive_id
      t.attachment :main_file
    end
  end
end
